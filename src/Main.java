import java.util.*;


public class Main {
    public static void main(String[] args) {
        Random numRam = new Random();
        int userGuess = numRam.nextInt(20);
        
        int numberCut = 20;
        int tries = 0 ;

        System.out.println("Hello! What is your name?");
        Scanner userName = new Scanner(System.in);
        String name = userName.nextLine();
        System.out.println("well " + name + " I am thinking of a number between 1 and 20. Take a guess");

//        where the user will enter their name
        System.out.println("Enter a number");
        Scanner userNumber = new Scanner(System.in);
        int guesses = userNumber.nextInt();

        while(guesses > numberCut && tries < 3) {
            System.out.println("Invalid number. Try again!");
            guesses = userNumber.nextInt();
            tries++;
        }
            while(guesses != userGuess && tries < 5 ) {
                 if(guesses <= userGuess) {
                    System.out.println("Guess too low!");
                    System.out.println("Take a guess");
                    guesses = userNumber.nextInt();
                }
                else if(guesses >= userGuess) {
                    System.out.println("Guess too high!");
                    System.out.println("Take a guess");
                    guesses = userNumber.nextInt();
                }

                tries++;
            }

        if(guesses == userGuess) {
            System.out.println("Good job, " + name + "!" +" You guessed my number in " + tries+ " guesses!");
        }

    }
}
